#! /usr/bin/env python3

""" Class 1 - Exercise 4 """

from nornir import InitNornir
from nornir_utils.plugins.functions import print_result
from nornir.core.task import Task, Result

# // Initialise Nornir object //
nr = InitNornir(config_file="config.yaml")

# // Tasks //
def hello_world(task: Task) -> Result:
    """ Simple Hello World Task """
    output=f"Hello World!! from {task.host.hostname}"
    return Result(host=task.host, result=output)

# // main() function //
def main():
    """ main function """
    result = nr.run(task=hello_world)
    print_result(result)

# // Execute task //
if __name__ == "__main__":
    main()


