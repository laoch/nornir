#! /usr/bin/env python3

""" Class 1 - Exercise 5 """

from nornir import InitNornir
from nornir_utils.plugins.functions import print_result
from nornir.core.task import Task, Result

# // Initialise Nornir object //
nr = InitNornir(config_file="config.yaml")

# // Tasks //
def hello_world(task: Task) -> Result:
    """ Simple Hello World Task """
    n = task.host.hostname
    d1 = task.host['dns1']
    d2 = task.host['dns2']
    output=f"Hostname: {n}\nDNS1: {d1}\nDNS2: {d2}"
    return Result(host=task.host, result=output)

# // main() function //
def main():
    """ main function """
    result = nr.run(task=hello_world)
    print_result(result)

# // Execute task //
if __name__ == "__main__":
    main()


