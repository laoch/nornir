#! /usr/bin/env python3

""" Class 1 - Exercise 2 """

from nornir import InitNornir
#from nornir_utils.plugins.functions import print_result
#from nornir.core.task import Task, Result

# // Initialise Nornir object //
nr = InitNornir(config_file="config.yaml")

# // main() function //
def main():
    """ main function """
    for k, v in nr.inventory.hosts.items():
        list_ = ['hostname', 'groups', 'platform', 
                 'username', 'password', 'port']
        print(k)
        for x in list_:
            print(f"{x}:", eval(f"v.{x}"))
        print("---\n")

# // Execute task //
if __name__ == "__main__":
    main()
