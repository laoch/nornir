#!/usr/bin/env python

""" Class 1 - Exercise 1 """
 
from nornir import InitNornir

# // Instantiate Nornir Class - 'nornir.core.Nornir' //
nr = InitNornir()

print(dir(nr))
print()
print(dir(nr.inventory))
print()
print(dir(nr.inventory.hosts))
print()
# // Print the Nornir inventory //
print(nr.inventory)

# // Print the Nornir inventory hosts //
print("nr.inventory.hosts                      :", nr.inventory.hosts)
print("nr.inventory.hosts['my_host']           :", nr.inventory.hosts["my_host"])
print("nr.inventory.hosts['my_host'].hostname  :", nr.inventory.hosts["my_host"].hostname)
