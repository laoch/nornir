#!/usr/bin/env python3

"""Class 5 - Exercise 3"""

from nornir import InitNornir
from nornir_utils.plugins.tasks.data import load_yaml


def srx_acl(task):
    """SRX Junos ACL function""" 
    rules_list = list()
    preamble = "set firewall family inet filter"

    # Load the YAML-ACL entries
    multi_acl = task.run(task=load_yaml, file="acl.yaml")
    acl_dict = multi_acl[0].result
    for acl_k, acl_v in acl_dict.items():
        for acl in acl_v:
            rules_list.append(f"{preamble} {acl_k} term {acl['term_name']} "
                              f"from protocol {acl['protocol']}")
            rules_list.append(f"{preamble} {acl_k} term {acl['term_name']} "
                              f"from destination-port {acl['destination_port']}")
            rules_list.append(f"{preamble} {acl_k} term {acl['term_name']} "
                              f"from destination-address {acl['destination_address']}")
            rules_list.append(f"{preamble} {acl_k} term {acl['term_name']} "
                              f"then {acl['state']}")

    # // Output the rules list //
    [print(x) for x in rules_list]
    print()


def main():
    nr = InitNornir(config_file="config.yaml")
    nr = nr.filter(name="srx2")
    nr.run(task=srx_acl)


if __name__ == "__main__":
    main()
