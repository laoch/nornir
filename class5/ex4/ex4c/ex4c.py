#!/usr/bin/env python3

"""Class 5 - Exercise 4c"""

from nornir import InitNornir
from nornir_utils.plugins.tasks.data import load_yaml
from nornir_jinja2.plugins.tasks import template_file

acl_path = "./"
acl_j2 = "acl.j2"


def srx_acl(task):
    """SRX Junos ACL function""" 
    rules_list = list()
    preamble = "set firewall family inet filter"

    # Load the YAML-ACL entries
    multi_acl = task.run(task=load_yaml, file="acl.yaml")
    acl_dict = multi_acl[0].result
    multi_result = task.run(task=template_file, path= acl_path, 
                            template=acl_j2, acls=acl_dict)
    task.host["acl"] = multi_result[0].result

    # // Output the rules list //
    print(task.host["acl"])
    print()


def main():
    with InitNornir(config_file="config.yaml") as nr:
        nr = nr.filter(name="srx2")
        nr.run(task=srx_acl)


if __name__ == "__main__":
    main()
