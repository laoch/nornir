#!/usr/bin/env python3

"""Class 5 - Exercise 4a"""

from nornir import InitNornir
from nornir_utils.plugins.tasks.data import load_yaml
from nornir_jinja2.plugins.tasks import template_string


ACL_TEMPLATE = """
{%- for acl, rules in acls.items() %}
  {%- for entry in rules %}

set firewall family inet filter {{ acl }} term {{ entry['term_name'] }} from protocol {{ entry['protocol'] }}
set firewall family inet filter {{ acl }} term {{ entry['term_name'] }} from destination-port {{ entry['destination_port'] }}
set firewall family inet filter {{ acl }} term {{ entry['term_name'] }} from destination-address {{ entry['destination_address'] }}
set firewall family inet filter {{ acl }} term {{ entry['term_name'] }} then {{ entry['state'] }}

  {%- endfor %}
{%- endfor %}"""  # noqa


def srx_acl(task):
    """SRX Junos ACL function""" 
    rules_list = list()
    preamble = "set firewall family inet filter"

    # Load the YAML-ACL entries
    multi_acl = task.run(task=load_yaml, file="acl.yaml")
    acl_dict = multi_acl[0].result
    multi_result = task.run(task=template_string, template=ACL_TEMPLATE, acls=acl_dict)

    # // Output the rules list //
    print(multi_result[0].result)
    print()


def main():
    with InitNornir(config_file="config.yaml") as nr:
        nr = nr.filter(name="srx2")
        nr.run(task=srx_acl)


if __name__ == "__main__":
    main()
