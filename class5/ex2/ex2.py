#!/usr/bin/env python3

"""Class 5 - Exercise 2"""

from getpass import getpass
from nornir import InitNornir
from nornir.core.filter import F
from nornir_napalm.plugins.tasks import napalm_get
from nornir_utils.plugins.functions import print_result


def main():
    """main function"""
    nr = InitNornir(config_file="config.yaml")
    nr = nr.filter(F(groups__contains="eos"))
    good_pass = getpass(prompt="Enter good pass for EOS devices: ")

    # // Change the password to valid one //
    for hostname, host_obj in nr.inventory.hosts.items():
        #print(f"Bad pass: {host_obj.password}")
        host_obj.password = good_pass
        #print(f"Good pass: {host_obj.password}")

    agg_result = nr.run(task=napalm_get, getters=["config"])
    print_result(agg_result)


if __name__ == "__main__":
    main()