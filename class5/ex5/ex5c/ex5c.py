#!/usr/bin/env python3

"""Class 5 - Exercise 5c"""

from nornir import InitNornir
from nornir.core.filter import F
from nornir_jinja2.plugins.tasks import template_file
from nornir_utils.plugins.functions import print_result
from nornir_utils.plugins.tasks.files import write_file
from nornir_napalm.plugins.tasks import napalm_get, napalm_configure

path_nxos = "./nxos"
path_conf = "./rendered_configs"
bgp_j2 = "bgp.j2"
int_j2 = "interface.j2"
red = "\033[0;32m"
green = "\033[0;32m"
reset = "\033[0;0m"

def render_conf(task):
    """Render Configurations"""
    iface = task.run(task=template_file, 
                     template=int_j2, 
                     path=path_nxos, 
                     **task.host)
    bgp = task.run(task=template_file, 
                   template=bgp_j2,
                   path=path_nxos, 
                   **task.host)
    task.host["iface_config"] = iface.result
    task.host["bgp_config"] = bgp.result


def write_conf(task):
    """Write Configurations"""
    task.run(task=write_file,
             filename=f"{path_conf}/{task.host}_intf",
             content=task.host["iface_config"])
    task.run(task=write_file,
             filename=f"{path_conf}/{task.host}_bgp",
             content=task.host["bgp_config"])


def deploy_conf(task):
    """Deploy Configurations"""
    task.run(task=napalm_configure, 
             configuration=task.host["iface_config"])
    task.run(task=napalm_configure, 
             configuration=task.host["bgp_config"])


def verify_conf(task):
    """validate the BGP config to verify"""
    result = task.run(task=napalm_get, getters=["bgp_neighbors"])
    bgp_neighbours = result.result["bgp_neighbors"]
    peer_ip = task.host["bgp_peer"]
    peer_state = bgp_neighbours["global"]["peers"][peer_ip]["is_up"]
    print(f"BGP state of {task.host} is: ", end="")
    print(f"{green}up{reset}" if peer_state else f"{red}down{reset}")


def main():
    with InitNornir(config_file="config.yaml") as nr:
        nr = nr.filter(F(groups__contains="nxos"))
        agg_result = nr.run(task=render_conf)
        print_result(agg_result)
        agg_result = nr.run(task=write_conf)
        print_result(agg_result)
        agg_result = nr.run(task=deploy_conf)
        print_result(agg_result)
        nr.run(task=verify_conf)


if __name__ == "__main__":
    main()
