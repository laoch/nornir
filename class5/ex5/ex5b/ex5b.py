#!/usr/bin/env python3

"""Class 5 - Exercise 5b"""

from nornir import InitNornir
from nornir.core.filter import F
from nornir_jinja2.plugins.tasks import template_file
from nornir_utils.plugins.functions import print_result
from nornir_utils.plugins.tasks.files import write_file

path_nxos = "./nxos"
path_conf = "./rendered_configs"
int_j2 = "interface.j2"
bgp_j2 = "bgp.j2"


def render_conf(task):
    """Render Configurations"""
    iface = task.run(task=template_file, 
                     template=int_j2, 
                     path=path_nxos, 
                     **task.host)
    bgp = task.run(task=template_file, 
                   template=bgp_j2,
                   path=path_nxos, 
                   **task.host)
    task.host["iface_config"] = iface.result
    task.host["bgp_config"] = bgp.result


def write_conf(task):
    """Write Configurations"""
    task.run(task=write_file,
             filename=f"{path_conf}/{task.host}_intf",
             content=task.host["iface_config"])
    task.run(task=write_file,
             filename=f"{path_conf}/{task.host}_bgp",
             content=task.host["bgp_config"])
    

def main():
    with InitNornir(config_file="config.yaml") as nr:
        nr = nr.filter(F(groups__contains="nxos"))
        agg_result = nr.run(task=render_conf)
        print_result(agg_result)
        agg_result = nr.run(task=write_conf)
        print_result(agg_result)


if __name__ == "__main__":
    main()
