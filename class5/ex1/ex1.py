#!/usr/bin/env python3

"""Class 5 - Exercise 1"""

import sys
from nornir import InitNornir
from nornir.core.filter import F
from nornir_netmiko import netmiko_send_config
from nornir_utils.plugins.functions import print_result


def conf_snmp_id(task):
    """Set the SNMP ID"""

    if task.host.platform == "ios":
        cmd = f"snmp-server chassis-id {task.host['snmp_id']}"
    elif task.host.platform == "eos":
        cmd = f"snmp chassis-id {task.host['snmp_id']}-{task.host.name}"
    else:
        print(f"Error: not configuring {task.host.platform} platforms")
        sys.exit(1)
    
    # // Run the task //
    task.run(task=netmiko_send_config, 
             config_commands=[cmd])


def main():
    nr = InitNornir(config_file="config.yaml")
    nr = nr.filter(F(groups__contains="eos") | 
                   F(groups__contains="ios"))
    agg_result = nr.run(task=conf_snmp_id)
    print_result(agg_result)


if __name__ == "__main__":
    main()