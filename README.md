# Nornir

## Class 1 - Nornir Fundamentals
* Overview
* Nornir's System Components
* Inventory System
* Inventory Example
* Plugins and Tasks
* First Nornir Task
* Concurrency and .run()

## Class 2: Config Options, Handling Results, Simple Tasks
  * Nornir Config Options
  * Introduction to Results Objects
  * Simple Netmiko Task
  * Simple NAPALM Tasks
  * Failed Tasks (Part1)

## Class 3: Inventory Expanded, Tasks Expanded
  * Inventory System Expanded
  * Inventory Preference
  * Inventory Filtering
  * Basic Nornir Inspection (Pdb)
  * Netmiko + TextFSM
  * NAPALM: Expanded Getters

## Class 4: Custom Tasks, Configuration Tasks
  * Custom Tasks
  * Results and Handling Custom Tasks
  * Netmiko File Copy
  * Netmiko Configuration Operations
  * NAPALM Configuration Operations

## Class 5: Inventory Data/ConnectionOptions, Jinja2 and Nornir
  * Inventory System - Generic Data
  * Inventory System - ConnectionOptions
  * Loading Additional Data
  * Jinja2 templating with Nornir
  * Jinja2 and Pushing Configurations
  * Closing Connections

## Class 6: Failed Tasks, Keys and Secrets, Troubleshooting
  * Failed Tasks and Exceptions (Part2)
  * Managing Keys and Secrets
  * Environment Variables
  * Nornir Logging
  * Troubleshooting and Debugging

