#!/usr/bin/env python3

"""Class 3 - Exercise 1b"""

from nornir import InitNornir


def main():
    nr = InitNornir(config_file="config.yaml")
    [print(f"{x} Timezone: {nr.inventory.hosts[x]['timezone']}") for x in nr.inventory.hosts]


if __name__ == "__main__":
    main()
