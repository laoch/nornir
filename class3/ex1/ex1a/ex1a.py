#!/usr/bin/env python3

"""Class 3 - Exercise 1a"""

from nornir import InitNornir


def main():
    nr = InitNornir(config_file="config.yaml")
    print(f"arista3: {nr.inventory.hosts['arista3'].data}\n")
    print(f"{dict(nr.inventory.hosts['arista3'].items())}\n")


if __name__ == "__main__":
    main()
