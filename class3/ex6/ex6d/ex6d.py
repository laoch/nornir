#!/usr/bin/env python3

"""Class 3 - Exercise 6d"""

from pprint import pprint
from nornir import InitNornir
from nornir.core.filter import F
from nornir_napalm.plugins.tasks import napalm_get


def main():

    c_data = {}
    nr = InitNornir(config_file="config.yaml")

    nr = nr.filter(F(groups__contains="nxos"))
    agg_result = nr.run(
        task=napalm_get, getters=["config", "facts"],
        getters_options={"config": {"retrieve": "all"}},
    )

    for dev_name, multi_result in agg_result.items():

        c_data[dev_name] = {}
        dev_result = multi_result[0]

        config_get = dev_result.result["config"]

        # // Ignore early lines //
        start_cfg = config_get["startup"].split("\n")[4:]
        run_cfg = config_get["running"].split("\n")[4:]

        # Get "facts"
        fact_get = dev_result.result["facts"]

        # Add to c_data 
        if run_cfg == start_cfg:
            c_data[dev_name]["start_run_match"] = True
        else:
            c_data[dev_name]["start_run_match"] = False

        c_data[dev_name]["vendor"] = fact_get["vendor"]
        c_data[dev_name]["model"] = fact_get["model"]
        c_data[dev_name]["uptime"] = fact_get["uptime"]

    pprint(c_data)

if __name__ == "__main__":
    main()

