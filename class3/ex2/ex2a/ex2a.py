#!/usr/bin/env python3

"""Class 3 - Exercise 2a"""

from nornir import InitNornir


def main():
    nr = InitNornir(config_file="config.yaml")

    arista1 = nr.filter(name="arista1")
    print(arista1.inventory.hosts)


if __name__ == "__main__":
    main()
