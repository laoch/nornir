#!/usr/bin/env python3

"""Class 3 - Exercise 2c"""

from nornir import InitNornir
from nornir.core.filter import F


def main():
    nr = InitNornir(config_file="config.yaml")

    sfo_obj = nr.filter(F(groups__contains="sfo"))
    print(sfo_obj.inventory.hosts)

if __name__ == "__main__":
    main()
