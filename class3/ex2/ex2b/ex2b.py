#!/usr/bin/env python3

"""Class 3 - Exercise 2b"""

from nornir import InitNornir


def main():
    nr = InitNornir(config_file="config.yaml")

    wan_devices = nr.filter(role="WAN")
    print(wan_devices.inventory.hosts)
    wan_devices_ssh = wan_devices.filter(port=22)
    print(wan_devices_ssh.inventory.hosts)

if __name__ == "__main__":
    main()
