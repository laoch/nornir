#!/usr/bin/env python3

"""Class 3 - Exercise 5a"""

## Ran with: python3 -m pdbr ex5a.py 

from pprint import pprint
from nornir import InitNornir
from nornir.core.filter import F
from nornir_netmiko import netmiko_send_command

import pdbr

def main():
    nr = InitNornir(config_file="config.yaml")

    dict_ = {}

    nr = nr.filter(F(groups__contains="eos"))
    agg_result = nr.run(
        task=netmiko_send_command, command_string="show int status", use_textfsm=True
    )
    
    for dev_name, multi_result in agg_result.items():
        dict_[dev_name] = {}
        device_result = multi_result[0]
        for intf_dict in device_result.result:
            intf_name = intf_dict["port"]
            inner_dict = {}
            inner_dict["status"] = intf_dict["status"]
            inner_dict["vlan"] = intf_dict["vlan"]
            dict_[dev_name][intf_name] = inner_dict
    pprint(dict_)


if __name__ == "__main__":
    main()
