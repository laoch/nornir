#!/usr/bin/env python3

"""Class 3 - Exercise 3a"""

from nornir import InitNornir
from nornir.core.filter import F


def main():
    nr = InitNornir(config_file="config.yaml")

    agg_devices = nr.filter(F(role__contains="AGG"))
    print(agg_devices.inventory.hosts)


if __name__ == "__main__":
    main()
