#!/usr/bin/env python3

"""Class 3 - Exercise 3d"""

from nornir import InitNornir
from nornir.core.filter import F


def main():
    nr = InitNornir(config_file="config.yaml")

    not_racecar = nr.filter(
        ~F(site_details__wifi_password__contains="racecar") & F(role="WAN")
    )
    print(not_racecar.inventory.hosts)


if __name__ == "__main__":
    main()
