#!/usr/bin/env python3

"""Class 3 - Exercise 4a"""

from nornir import InitNornir
from nornir.core.filter import F


def main():
    nr = InitNornir(config_file="config.yaml")

    print(nr.inventory.hosts)


if __name__ == "__main__":
    main()
