#!/usr/bin/env python3

"""Class 3 - Exercise 4b"""

from nornir import InitNornir
from nornir.core.filter import F
from nornir_netmiko import netmiko_send_command

def main():
    nr = InitNornir(config_file="config.yaml")

    nr = nr.filter(F(groups__contains="eos"))
    agg_result = nr.run(
        task=netmiko_send_command, command_string="show int status", use_textfsm=True
    )

    print(type(agg_result["arista1"][0].result), agg_result["arista1"][0].result)


if __name__ == "__main__":
    main()
