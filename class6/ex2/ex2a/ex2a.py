#! /usr/bin/env python3

""" Class 6 - Exercise 2a"""

from nornir import InitNornir
from nornir.core.filter import F
from nornir_utils.plugins.functions import print_result
from nornir_jinja2.plugins.tasks import template_file


def render_config(task):
    """Render the configuration"""
    task.run(task=template_file,
             template="loopback.j2",
             path="./",
             **task.host)


def main():
    """main() function"""
    with InitNornir(config_file="config.yaml") as nr:
        nr = nr.filter(F(groups__contains="nxos"))
        agg_result = nr.run(task=render_config)
    print_result(agg_result)


if __name__ == "__main__":
    main()

