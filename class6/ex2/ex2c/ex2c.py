#! /usr/bin/env python3

""" Class 6 - Exercise 2c"""

from nornir import InitNornir
from nornir.core.filter import F
from nornir.core.task import Result
from nornir.core.exceptions import NornirSubTaskError
from nornir_jinja2.plugins.tasks import template_file
from nornir_utils.plugins.functions import print_result


def render_config(task):
    """Render the configuration"""
    try:
        task.run(task=template_file,
                 template="loopback.j2",
                 path="./",
                 **task.host)
        return f"Template rendered successfully for device {task.host}"
    except NornirSubTaskError:
        task.results.pop()
        msg = f"Error with the template for the device {task.host}"
        return Result(changed=False, diff=None,
                      host=task.host,result=msg,
                      failed=False, exception=None,)


def main():
    """main() function"""
    with InitNornir(config_file="config.yaml") as nr:
        nr = nr.filter(F(groups__contains="nxos"))
        agg_result = nr.run(task=render_config)
    print_result(agg_result)


if __name__ == "__main__":
    main()

