#! /usr/bin/env python3

""" Class 6 - Exercise 2b2"""

from nornir import InitNornir
from nornir.core.filter import F
from nornir_utils.plugins.functions import print_result
from nornir_jinja2.plugins.tasks import template_file
from nornir.core.exceptions import NornirSubTaskError


def render_config(task):
    """Render the configuration"""
    try:
        task.run(task=template_file,
                 template="loopback.j2",
                 path="./",
                 **task.host)
        return f"Template rendered successfully for device {task.host}"
    except NornirSubTaskError:
        return f"Error with the template for the device {task.host}"


def main():
    """main() function"""
    with InitNornir(config_file="config.yaml") as nr:
        nr = nr.filter(F(groups__contains="nxos"))
        agg_result = nr.run(task=render_config)
    print_result(agg_result)


if __name__ == "__main__":
    main()

