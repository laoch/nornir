#! /usr/bin/env python3

""" Class 6 - Exercise 4a"""

import os
from nornir import InitNornir
from nornir.core.filter import F
from nornir_netmiko import netmiko_send_command
from nornir_utils.plugins.functions import print_result

password = os.environ.get("NORNIR_PASSWORD", "defaultpass") 


def send_cmd(task):
    """Send Command and log"""
    cmd = "show running-config | inc hostname"
    log = f"{task.host}_session_logger.log"
    con_op = task.host.groups[0].connection_options
    extras = con_op["netmiko"].extras
    extras["session_log"] = log
    task.run(task=netmiko_send_command,
             command_string=cmd)


def main():
    """main() function"""
    with InitNornir(config_file="config.yaml") as nr:
        nr = nr.filter(F(groups__contains="nxos"))
        for hostname, host_obj in nr.inventory.hosts.items():
            host_obj.password = password
        agg_result = nr.run(task=send_cmd)
        print_result(agg_result)


if __name__ == "__main__":
    main()

