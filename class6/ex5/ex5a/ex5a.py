#! /usr/bin/env python3

""" Class 6 - Exercise 5a"""

from nornir import InitNornir
from nornir_utils.plugins.functions import print_result
from nornir_netmiko import netmiko_send_command


def send_cmd(task):
    """Send Command"""
    def_cmd = "show clock"
    cmd_map = {"junos": "show system uptime"}
    cmd = cmd_map.get(task.host.platform, def_cmd)
    task.run(task=netmiko_send_command,
             command_string=cmd)


def main():
    """main() function"""
    with InitNornir(config_file="config.yaml") as nr:
        agg_result = nr.run(task=send_cmd)
    print_result(agg_result)


if __name__ == "__main__":
    main()

