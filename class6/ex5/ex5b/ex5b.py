#! /usr/bin/env python3

""" Class 6 - Exercise 5b"""

from nornir import InitNornir
from nornir.core.exceptions import NornirSubTaskError
from nornir_utils.plugins.functions import print_result
from nornir_netmiko import netmiko_send_command
from random import choice

password = os.environ.get("NORNIR_PASSWORD", "goodpass") 


def send_cmd(task):
    """Send Command"""
    def_cmd = "show clock"
    cmd_map = {"junos": "show system uptime"}
    cmd = cmd_map.get(task.host.platform, def_cmd)
    try:
        task.run(task=netmiko_send_command,
                 command_string=cmd)
    except NornirSubTaskError as e: 
        err = "Authentication to device failed"
        if e.result.exception.args[0].startswith(err):
            task.results.pop()
            task.host.password = password
            task.run(task=netmiko_send_command,
                 command_string=cmd)


def main():
    """main() function"""
    with InitNornir(config_file="config.yaml") as nr:
        for hostname, host_obj in nr.inventory.hosts.items():
            if choice([0, 1]):
                host_obj.password = "badpass"
        agg_result = nr.run(task=send_cmd)
    print_result(agg_result)


if __name__ == "__main__":
    main()

