#! /usr/bin/env python3

"""Simple Crypt Decrypt"""

import sys
import yaml
from cryptography.fernet import Fernet
from pprint import pprint


KEY_FILE = "secret.key"
PT_FILE = "passwd.yaml"
CT_FILE = "passwd.yaml.crypt"


def key_gen(key_file):
    """Key generator function"""
    fk = Fernet.generate_key()
    with open(f"{key_file}", "wb") as fh:
        fh.write(fk)
    return f"Key generated as {key_file}"


def encryptf(key_file, pt_file, ct_file):
    """Encrypt a file"""
    with open(f"{key_file}", "rb") as fh:
        fernet = Fernet(fh.read())
    with open(pt_file, "rb") as fh:
        plain_msg = fh.read()
    encrypt_msg = fernet.encrypt(plain_msg)
    with open(ct_file, "wb") as fh:
        fh.write(encrypt_msg)
    return f"{pt_file} encrypted as as {ct_file}"


def decryptf(key_file, ct_file):
    """Access username/password information in crypt"""
    dict_ = dict()
    with open(key_file, "rb") as k:
        fernet = Fernet(k.read())
    with open(ct_file, "rb") as fh:
        encrypt_msg = fh.read()
        yaml_out = fernet.decrypt(encrypt_msg).decode("utf-8")
    dict_ = yaml.safe_load(yaml_out)
    return(dict_)


def main():
    """main() function"""
    cmd = "show version"    
    while True:
        selection = input("(k)ey generation, (e)ncrypt or (d)crypt: ")[0]
        if selection in ("ked"):
            break
    if selection == 'k':
        print(key_gen(KEY_FILE))
    elif selection == 'e':
        print(encryptf(KEY_FILE, PT_FILE, CT_FILE))
    elif selection == 'd':
        print(decryptf(KEY_FILE, CT_FILE))


if __name__ == "__main__":
    main()
