#! /usr/bin/env python3

""" Class 6 - Exercise 1a"""

from nornir import InitNornir
from nornir_utils.plugins.functions import print_result
from nornir_netmiko import netmiko_send_command

device = "srx2"

def send_cmd(task):
    """Send Netmiko command"""
    task.run(task=netmiko_send_command,
             command_string="show ip interface")


def main():
    """main() function"""
    with InitNornir(config_file="config.yaml") as nr:
        nr = nr.filter(name=device)
        agg_result = nr.run(task=send_cmd)
    print_result(agg_result)

if __name__ == "__main__":
    main()

