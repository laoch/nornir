#! /usr/bin/env python3

""" Class 6 - Exercise 3b"""

import os
import logging
from getpass import getpass
from nornir import InitNornir
from nornir.core.filter import F
from nornir_netmiko import netmiko_send_command
from nornir_utils.plugins.functions import print_result

LOGGER = logging.getLogger("nornir")

password = "goodpass" #getpass("Enter the password: ")


def main():
    """main() function"""
    LOGGER.info("Before instantiating Nornir.")    
    with InitNornir(config_file="config.yaml") as nr:
        nr = nr.filter(F(groups__contains="ios"))
        for hostname, host_obj in nr.inventory.hosts.items():
            host_obj.password = password
        LOGGER.critical("Approaching the critical bit.") 
        cmd = "show running-config | inc hostname"
        agg_result = nr.run(task=netmiko_send_command,
                            command_string=cmd)
        LOGGER.error("Huh Huh An error.") 
    print_result(agg_result)
    LOGGER.error("A debug may be necessary.") 


if __name__ == "__main__":
    main()

