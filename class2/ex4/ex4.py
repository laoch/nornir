#! /usr/bin/env python3

""" Class 2 - Exercise 4 """

from nornir import InitNornir
from nornir.core.filter import F
from nornir_napalm.plugins.tasks import napalm_get
from pprint import pprint

DEF_GW = "10.220.88.1"


def main():
    """main function"""

    # // Initialise Nornir object //
    nr = InitNornir(config_file="config.yaml")
    ios_filt = F(groups__contains="ios")
    eos_filt = F(groups__contains="eos")
    nr = nr.filter(ios_filt | eos_filt)
    cmd = "show ip arp"
    def_gw_list = list()
    my_results = nr.run(task=napalm_get, getters=["arp_table"])

    # // Breakdown the results //
    for host, multi_results in my_results.items():
        arp_table = multi_results[0].result
        # [print(x) for x in arp_table['arp_table']]
        for x in arp_table["arp_table"]:
            if x["ip"] == DEF_GW:
                def_gw_list.append([host, x])

    # // output //
    for x in def_gw_list:
        print(f"Host: {x[0]}, Gateway: {x[1]}")


if __name__ == "__main__":
    main()
