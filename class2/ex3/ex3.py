#! /usr/bin/env python3

""" Class 2 - Exercise 3 """

from nornir import InitNornir
from nornir.core.filter import F
from nornir_netmiko import netmiko_send_command
from pprint import pprint

DEF_GW = "10.220.88.1"


def main():
    """main function"""

    # // Initialise Nornir object //
    nr = InitNornir(config_file="config.yaml")
    ios_filt = F(groups__contains="ios")
    eos_filt = F(groups__contains="eos")
    nr = nr.filter(ios_filt | eos_filt)
    cmd = "show ip arp"
    def_gw_list = list()
    my_results = nr.run(task=netmiko_send_command, command_string=cmd)

    # // Breakdown the results //
    for host, multi_results in my_results.items():
        arp_table = multi_results[0].result
        for line in arp_table.splitlines():
            if DEF_GW in line:
                def_gw_list.append([host, line])
                break

    # // output //
    for x in def_gw_list:
        wp_str = str()
        wp_list = x[1].split()
        wp_str = " ".join(wp_list)
        print(f"Host: {x[0]}, Gateway: {wp_str}")


if __name__ == "__main__":
    main()
