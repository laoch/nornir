#! /usr/bin/env python3

""" Class 2 - Exercise 5d """

from nornir import InitNornir
from nornir.core.filter import F
from nornir_utils.plugins.functions import print_result
from nornir_netmiko import netmiko_send_command
from rich import print as rprint


def main():
    """main function"""

    # // Initialise Nornir object //
    nr = InitNornir(config_file="config.yaml")
    nr.inventory.hosts["cisco3"].password = "bogus"
    ios_filt = F(groups__contains="ios")
    nr = nr.filter(ios_filt)
    cmd = "show ip int brief"
    my_results = nr.run(task=netmiko_send_command, command_string=cmd)

    # // output A //
    print_result(my_results)
    rprint(f"Task level: {my_results.failed_hosts}")
    rprint(f"Global level: {nr.data.failed_hosts}")

    # // Recover password on Cisco 3 //
    nr.inventory.hosts["cisco3"].password = nr.inventory.defaults.password

    # // Rerun on failed hosts //
    my_results = nr.run(
        task=netmiko_send_command, command_string=cmd, on_good=False, on_failed=True
    )

    # // output B //
    str_ = "Output for Failed hosts"
    print(f"\n{'*' * len(str_)}\n{str_}\n{'*' * len(str_)}\n")
    print_result(my_results)
    rprint(f"Task level: {my_results.failed_hosts}")
    rprint(f"Global level: {nr.data.failed_hosts}")

    # // Recovering the failed hosts //
    str_ = "Recovering the failed hosts"
    print(f"\n{'*' * len(str_)}\n{str_}\n{'*' * len(str_)}\n")
    nr.data.recover_host("cisco3")
    rprint(f"Task level: {my_results.failed_hosts}")
    rprint(f"Global level: {nr.data.failed_hosts}")


if __name__ == "__main__":
    main()
