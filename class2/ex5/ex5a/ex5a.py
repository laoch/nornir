#! /usr/bin/env python3

""" Class 2 - Exercise 5a """

from nornir import InitNornir
from nornir.core.filter import F
from nornir_utils.plugins.functions import print_result
from nornir_netmiko import netmiko_send_command


def main():
    """main function"""

    # // Initialise Nornir object //
    nr = InitNornir(config_file="config.yaml")
    ios_filt = F(groups__contains="ios")
    nr = nr.filter(ios_filt)
    cmd = "show ip int brief"
    my_results = nr.run(task=netmiko_send_command, command_string=cmd)

    # // output //
    print_result(my_results)


if __name__ == "__main__":
    main()
