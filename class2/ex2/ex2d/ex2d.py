#! /usr/bin/env python3

""" Class 2 - Exercise 2d """

from nornir import InitNornir
from nornir.core.filter import F
from nornir_netmiko import netmiko_send_command
from rich import print as rprint


def main():
    """main function"""

    # // Initialise Nornir object //
    nr = InitNornir(config_file="config.yaml")
    filt = F(groups__contains="ios")
    nr = nr.filter(filt)
    cmd = "show running-config | include hostname"
    my_results = nr.run(task=netmiko_send_command, 
                        command_string=cmd)
    host_results = my_results['cisco3']
    task_result = host_results[0]
    rprint(type(task_result), task_result)
    rprint(f"Host: {task_result.host}")
    rprint(f"Name: {task_result.name}")
    rprint(f"Result: {task_result.result}")
    rprint(f"Failed: {task_result.failed}")


if __name__ == "__main__":
    main()
