#! /usr/bin/env python3

""" Class 2 - Exercise 2b """

from nornir import InitNornir
from nornir.core.filter import F
from nornir_netmiko import netmiko_send_command
from rich import print as rprint


def main():
    """main function"""

    # // Initialise Nornir object //
    nr = InitNornir(config_file="config.yaml")
    filt = F(groups__contains="ios")
    nr = nr.filter(filt)
    cmd = "show running-config | include hostname"
    my_results = nr.run(task=netmiko_send_command, 
                        command_string=cmd)
    rprint(f"\n{nr.inventory.hosts}\n")
    rprint(f"my_results type: {type(my_results)}\n")
    rprint(f"keys() method: {my_results.keys()}\n")
    rprint(f"items() method: {my_results.items()}\n")
    rprint(f"values() method: {my_results.values()}\n")
    rprint(type(my_results.values()))


if __name__ == "__main__":
    main()
