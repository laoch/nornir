#! /usr/bin/env python3

""" Class 2 - Exercise 2a """

from nornir import InitNornir
from nornir.core.filter import F


def main():
    """main function"""

    # // Initialise Nornir object //
    nr = InitNornir(config_file="config.yaml")
    filt = F(groups__contains="ios")
    nr = nr.filter(filt)
    print(nr.inventory.hosts)


if __name__ == "__main__":
    main()
