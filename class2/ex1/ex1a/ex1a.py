#! /usr/bin/env python3

""" Class 2 - Exercise 1a """

from nornir import InitNornir


def main():
    """main function"""

    # // Instantiate Nornir object //
    nr = InitNornir(config_file="config.yaml")
    print(nr.runner.num_workers)


if __name__ == "__main__":
    main()
