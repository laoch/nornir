#! /usr/bin/env python3

""" Class 2 - Exercise 1d """

from nornir import InitNornir


def main():
    """main function"""

    # // Initialise Nornir object //
    nr = InitNornir(config_file="config.yaml", 
                    runner={"plugin":"threaded", 
                            "options":{"num_workers": 15}})

    print(nr.runner.num_workers)


if __name__ == "__main__":
    main()
