#!/usr/bin/env python3

"""Class 4 - Exercise 2c"""

from nornir import InitNornir
from nornir.core.filter import F
from nornir_utils.plugins.functions import print_result
from nornir_netmiko import netmiko_file_transfer


def file_copy(task):
    """File copy function"""
    host, name, platform = task.host, task.host.name, task.host.platform
    file = host['file_name']
    dst_file = f"{platform}/{name}-saved.txt"

    # // SSH SCP file transfer //
    multi_result = task.run(task=netmiko_file_transfer,
                            direction="get",
                            source_file=file,
                            dest_file=dst_file,
                            overwrite_file=True)

    # // Test if file transferred OK //
    print(f"{file} from {host} transferred to",
          f"{dst_file}: {multi_result[0].result}")

 
def main():
    """Main function""" 
    nr = InitNornir(config_file="config.yaml")
    nr = nr.filter(F(groups__contains="eos"))
    nr.run(task=file_copy)


if __name__ == "__main__":
    main()
