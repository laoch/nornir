#!/usr/bin/env python3

"""Class 4 - Exercise 2b"""

from nornir import InitNornir
from nornir.core.filter import F
from nornir_netmiko import netmiko_file_transfer
from nornir_netmiko import netmiko_send_command


def file_copy(task):
    """File copy function"""
    host, platform = task.host, task.host.platform
    file = host['file_name']
    src_file = f"{platform}/{file}"

    # // SSH SCP file transfer //
    task.run(task=netmiko_file_transfer,
             direction="put",
             source_file=src_file,
             dest_file=file,
             overwrite_file=True)

    # // Verify file contents on EOS device //
    cmd = f"more flash:/{file}"
    multi_result = task.run(task=netmiko_send_command, 
                            command_string=cmd)
    print(f"{host}:> {cmd}\n{multi_result[0].result}\n")

 
def main():
    """Main function""" 
    nr = InitNornir(config_file="config.yaml")
    nr = nr.filter(F(groups__contains="eos"))
    nr.run(task=file_copy)


if __name__ == "__main__":
    main()
