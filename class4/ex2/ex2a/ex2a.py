#!/usr/bin/env python3

"""Class 4 - Exercise 2a"""

from nornir import InitNornir
from nornir.core.filter import F
from nornir_utils.plugins.functions import print_result
from nornir_netmiko import netmiko_file_transfer


def file_copy(task):
    """File copy function"""
    host, platform = task.host, task.host.platform
    file = host['file_name']
    src_file = f"{platform}/{file}"

    # // SSH SCP file transfer //
    task.run(task=netmiko_file_transfer,
             direction="put",
             source_file=src_file,
             dest_file=file,
             overwrite_file=True)


def main():
    """Main function""" 
    nr = InitNornir(config_file="config.yaml")
    nr = nr.filter(F(groups__contains="eos"))
    result = nr.run(task=file_copy)
    print_result(result)


if __name__ == "__main__":
    main()
