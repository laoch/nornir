#!/usr/bin/env python3

"""Class 4 - Exercise 5b"""

from nornir import InitNornir
from nornir_napalm.plugins.tasks import napalm_get
from nornir_utils.plugins.functions import print_result
from nornir_napalm.plugins.tasks import napalm_configure

device = "arista4"

def main():
    """Main function""" 
    nr = InitNornir(config_file="config.yaml")
    nr = nr.filter(name=device)
    agg_result = nr.run(task=napalm_get, 
                        getters=["config"], 
                        retrieve="running")
    dev_result = agg_result[device][0].result
    dev_running_config = dev_result["config"]["running"]

    # New config
    conf = (f"interface loopback123\n"
            f"  description verycoolloopback")
    agg_result = nr.run(task=napalm_configure, 
                        configuration=conf)
    print_result(agg_result)


if __name__ == "__main__":
    main()
