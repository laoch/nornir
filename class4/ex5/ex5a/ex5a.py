#!/usr/bin/env python3

"""Class 4 - Exercise 5a"""

from nornir import InitNornir
from nornir_napalm.plugins.tasks import napalm_get

device = "arista4"

def main():
    """Main function""" 
    nr = InitNornir(config_file="config.yaml")
    nr = nr.filter(name=device)
    agg_result = nr.run(task=napalm_get, 
                        getters=["config"], 
                        retrieve="running")
    dev_result = agg_result[device][0].result
    dev_running_config = dev_result["config"]["running"]

    file = f"{device}-running.txt"
    with open(file, "w") as fh:
        fh.write(dev_running_config)
    print()
    print(dev_running_config)


if __name__ == "__main__":
    main()
