#!/usr/bin/env python3

"""Class 4 - Exercise 3b"""

from nornir import InitNornir
from nornir.core.filter import F
from nornir_netmiko import netmiko_send_config
from nornir_netmiko import netmiko_send_command

vlan_id = "100"
vlan_name = "my_vlan"


def vlan_conf(task, vlan_id, vlan_name):
    """VLAN configuration function"""
    cmd = f"show vlan brief | include {vlan_id}"
    multi_result = task.run(task=netmiko_send_command, command_string=cmd)
    out = multi_result[0].result.split()
    if len(out) == 0:
        cmds = [f"vlan {vlan_id}", f"name {vlan_name}"]
        multi_result2 = task.run(task=netmiko_send_config, config_commands=cmds)
        if multi_result2[0].failed == False:
            print(f"VLAN: {vlan_name} with VLAN ID: {vlan_id}",
                  f"configured on {task.host}")
        else:
            print(f"Failed to configure VLAN: {vlan_name}",
                  f"with VLAN ID: {vlan_id} on {task.host}")

    elif out[0] == vlan_id and vlan_name == vlan_name:
        print(f"VLAN: {vlan_name} with VLAN ID: {vlan_id}",
              f"already configured on {task.host}")

    else:
        print(f"No idea how you got here on {task.host}")


def main():
    """Main function"""
    nr = InitNornir(config_file="config.yaml")
    nr = nr.filter(F(groups__contains="eos") | F(groups__contains="nxos"))
    result = nr.run(task=vlan_conf, vlan_id=vlan_id, vlan_name=vlan_name)


if __name__ == "__main__":
    main()
