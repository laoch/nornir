#!/usr/bin/env python3

"""Class 4 - Exercise 3c"""

from nornir import InitNornir
from nornir.core.filter import F
from nornir.core.task import Result
from nornir_utils.plugins.functions import print_result
from nornir_netmiko import netmiko_send_config
from nornir_netmiko import netmiko_send_command

vlan_id = "100"
vlan_name = "my_vlan"


def vlan_conf(task, vlan_id, vlan_name):
    """VLAN configuration function"""
    cmd = f"show vlan brief | include {vlan_id}"
    multi_result = task.run(task=netmiko_send_command, command_string=cmd)
    out = multi_result[0].result.split()
    changed = False
    failed = False
    if len(out) == 0:
        cmds = [f"vlan {vlan_id}", f"name {vlan_name}"]
        multi_result2 = task.run(task=netmiko_send_config, config_commands=cmds)
        if multi_result2[0].failed == False:
            changed = True
            result = (f"VLAN: {vlan_name} with VLAN ID: {vlan_id}"
                      f" configured on {task.host}")
            return Result(host=task.host, result=result, 
                          changed=changed, failed=failed)
        else:
            changed = False
            result = (f"Failed to configure VLAN: {vlan_name}"
                      f" with VLAN ID: {vlan_id} on {task.host}")
            return Result(host=task.host, result=result, 
                          changed=changed, failed=failed)

    elif out[0] == vlan_id and vlan_name == vlan_name:
        changed = False
        result = (f"VLAN: {vlan_name} with VLAN ID: {vlan_id}"
                  f" already configured on {task.host}")
        return Result(host=task.host, result=result, 
                      changed=changed, failed=failed)

    else:
        changed = False
        result = f"No idea how you got here on {task.host}"
        return Result(host=task.host, result=result, 
                      changed=changed, failed=failed)

def main():
    """Main function"""
    nr = InitNornir(config_file="config.yaml")
    nr = nr.filter(F(groups__contains="eos") | F(groups__contains="nxos"))
    result = nr.run(task=vlan_conf, vlan_id=vlan_id, vlan_name=vlan_name)
    print_result(result)

if __name__ == "__main__":
    main()
