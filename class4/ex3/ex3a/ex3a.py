#!/usr/bin/env python3

"""Class 4 - Exercise 3a"""

from nornir import InitNornir
from nornir.core.filter import F
from nornir_utils.plugins.functions import print_result
from nornir_netmiko import netmiko_send_config

vlan_id = "100"
vlan_name = "my_vlan"

def vlan_conf(task, vlan_id, vlan_name):
    """VLAN configuration function"""
    task.run(task=netmiko_send_config,
             config_commands=[f"vlan {vlan_id}", f"name {vlan_name}"])


def main():
    """Main function""" 
    nr = InitNornir(config_file="config.yaml")
    nr = nr.filter(F(groups__contains="eos") | F(groups__contains="nxos"))
    result = nr.run(task=vlan_conf, 
                    vlan_id=vlan_id, 
                    vlan_name=vlan_name)
    print_result(result)


if __name__ == "__main__":
    main()

