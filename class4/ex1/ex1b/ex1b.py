#!/usr/bin/env python3

"""Class 4 - Exercise 1b"""

from nornir import InitNornir
from nornir_netmiko import netmiko_send_command
import sys
import re

MIN = 60
HOUR = MIN * 60
DAY = HOUR * 24
WEEK = DAY * 7
YEAR = DAY * 365

def process_uptime(arg):
    """Process uptime"""

    uptime = arg.strip()
    
    if "Kernel uptime is" in uptime:
        list_ = re.findall("\s\d+?\s", uptime)
        list_ = [int(x.strip()) for x in list_]
        [d, h, m, s] = list_
        return DAY * d + HOUR * h + MIN * m + s
    elif "Uptime:" in uptime:
        list_ = re.findall("\s\d+?\s", uptime)
        list_ = [int(x.strip()) for x in list_]
        [w, d, h, m] = list_
        return WEEK * w + DAY * d + HOUR * h + MIN * m
    elif "uptime is" in uptime:        
        list_ = re.findall("\s\d+?\s", uptime)
        list_ = [int(x.strip()) for x in list_]
        if len(list_) == 5:
            [y, w, d, h, m] = list_
            return YEAR * y + WEEK * w + DAY * d + HOUR * h + MIN * m
        else:
            [w, d, h, m] = list_
            return WEEK * w + DAY * d + HOUR * h + MIN * m
    elif "System booted" in uptime:
        list_ = re.findall("\(.+\)", uptime)
        list_ = re.findall("\((\d+?)w(\d+?)d\s(\d+?):(\d+?)\sago\)", list_[0])
        [w, d, h, m] = [int(x.strip()) for x in list_[0]]
        #return WEEK * w + DAY * d + HOUR * h + MIN * m
        return 90
    else:
        sys.exit("Not a valid networking device")


def uptime_fun(task):
    """Get the uptime from each device"""

    cmd_dict = {
        "ios": "show version | include uptime",
        "eos": "show version | include Uptime",
        "nxos": "show version | include uptime",
        "junos": "show system uptime | match System",
    }
    # // task.host.platform ==> ios, eos, nxos, junos //
    cmd = cmd_dict[task.host.platform]
    task.run(task=netmiko_send_command, command_string=cmd)


def main():
    nr = InitNornir(config_file="config.yaml")
    agg_result = nr.run(task=uptime_fun)
    print()
    for hostname, multi_result in agg_result.items():
        ret_uptime = process_uptime(multi_result[1].result)
        if ret_uptime < DAY:
            print(f"\033[0;31m{hostname}: device rebooted recently: {ret_uptime}\033[0;0m")
        else:
            print(f"{hostname}: device uptime is: {ret_uptime}")
    print()


if __name__ == "__main__":
    main()