#!/usr/bin/env python3

"""Class 4 - Exercise 1a"""

from nornir import InitNornir
from nornir_netmiko import netmiko_send_command


def uptime_fun(task):
    cmd_dict = {
        "ios": "show version | include uptime",
        "eos": "show version | include Uptime",
        "nxos": "show version | include uptime",
        "junos": "show system uptime | match System",
    }
    # // task.host.platform ==> ios, eos, nxos, junos //
    cmd = cmd_dict[task.host.platform]
    task.run(task=netmiko_send_command, command_string=cmd)


def main():
    nr = InitNornir(config_file="config.yaml")
    agg_result = nr.run(task=uptime_fun)
    print()
    for hostname, multi_result in agg_result.items():
        print(f"{hostname}: {multi_result[1].result.strip()}")
    print()


if __name__ == "__main__":
    main()