#!/usr/bin/env python3

"""Class 4 - Exercise 4b"""

from nornir import InitNornir
from nornir.core.filter import F
from nornir_utils.plugins.functions import print_result
from nornir_napalm.plugins.tasks import napalm_configure

vlan_id = "100"
vlan_name = "my_vlan"
dry_run = False

def vlan_conf(task, vlan_id, vlan_name, dry_run=False):
    """VLAN configuration function"""
    cmd_str = f"vlan {vlan_id}\nname {vlan_name}"
    task.run(task=napalm_configure, 
             configuration=cmd_str,
             dry_run=dry_run)


def main():
    """Main function""" 
    nr = InitNornir(config_file="config.yaml")
    nr = nr.filter(F(groups__contains="eos") | F(groups__contains="nxos"))
    result = nr.run(task=vlan_conf, 
                    vlan_id=vlan_id, 
                    vlan_name=vlan_name,
                    dry_run=dry_run)
    print_result(result)


if __name__ == "__main__":
    print(f"DRY RUN: {dry_run}")
    main()

